--- Initial Clientside Code for FNWVars.
-- @module FNWVars.Client

-- Add sends the EntIndex, uinteger key, string key, type id, and a value of supported type
-- Update sends the EntIndex, uinteger key, type id, and a value of supported type
-- Remove sends the EntIndex and the string key
-- Keymap sends the uinteger key and its string key pair
local dprint      = fnwvar_debug_print
local UIntKeyBits = _FNWVars.UIntKeyBits
local TypeIDBits  = _FNWVars.TypeIDBits
local EntIDBits   = _FNWVars.EntIDBits

_FNWVars.PendingFNWVars = _FNWVars.PendingFNWVars or {} -- Table to hold variables that are pending a keyid/keystr lookup

--- Updates/creates FNWVar data clientside
-- @realm Client
-- @param entid The entity index of the entity that has a changed FNWVar
-- @param key The FNWVar string key
-- @param value The FNWVar's new value
local function ReceivedFNWVarData(entid, key, value)
    assert(type(entid) == "number", "[FNWVars] ReceivedFNWVarData called with invalid entity")
    assert(#key > 0,                "[FNWVars] ReceivedFNWVarData called with invalid/empty string")

    local entkeys = _FNWVars.Entities[entid]
    if (not entkeys) then -- Create FNWVar entity table for this entity
        entkeys = {}
        _FNWVars.Entities[entid] = entkeys
    end

    local keyinfo = entkeys[key]
    if (not keyinfo) then -- Create FNWVar key for this entity
        keyinfo = {
            recipients = {LocalPlayer()}
        }
        entkeys[key] = keyinfo
    end

    local oldvalue       = keyinfo.value
    keyinfo.value        = value
    keyinfo.last_updated = CurTime()

    -- Execute callbacks
    local ent = Entity(entid)
    local callbacks = _FNWVars.Callbacks[key]
    for unique_id, callfunc in pairs(_FNWVars.Callbacks["*"] or {}) do -- Wildcard callbacks
        callfunc(ent, key, oldvalue, value)
    end
    if (callbacks) then
        for unique_id, callfunc in pairs(callbacks) do
            callfunc(ent, key, oldvalue, value)
        end
    end
end


net.Receive("fnwvar_add", function(len)
    local entid   = net.ReadInt(EntIDBits)
    local intkey  = net.ReadUInt(UIntKeyBits)
    local strkey  = net.ReadString()
    local typeid  = net.ReadUInt(TypeIDBits)
    local typestr = _FNWVars.TypeLookup[typeid]
    local value   = nil

    if (not _FNWVars.KeyLookup[intkey]) then
        _FNWVars.KeyLookup[intkey] = strkey
    end
    if (not _FNWVars.KeyLookup[strkey]) then
        _FNWVars.KeyLookup[strkey] = intkey
    end

    if (typestr) then
        local typeio = _FNWVars.NetIO[typestr]
        value = typeio.read()

        ReceivedFNWVarData(entid, strkey, value)

        dprint(string.format("fnwvar_add %s %s %s %i", Entity(entid), strkey, value, len))
    end
end)


net.Receive("fnwvar_update", function(len)
    local entid   = net.ReadInt(EntIDBits)
    local intkey  = net.ReadUInt(UIntKeyBits)
    local strkey  = _FNWVars.KeyLookup[intkey]
    local typeid  = net.ReadUInt(TypeIDBits)
    local typestr = _FNWVars.TypeLookup[typeid]
    local value   = nil

    if (typestr) then
        local typeio = _FNWVars.NetIO[typestr]
        value = typeio.read()

        if (strkey) then
            ReceivedFNWVarData(entid, strkey, value)
        else
            -- We don't know the string value of this integer key, add to a pending table and request info
            local pendingentvars = _FNWVars.PendingFNWVars[entid]
            if (not pendingentvars) then
                pendingentvars = {}
                _FNWVars.PendingFNWVars[entid] = pendingentvars
            end

            pendingentvars[#pendingentvars+1] = {
                intkey       = intkey,
                strkey       = strkey,
                typeid       = typeid,
                typestr      = typestr,
                value        = value,
                last_updated = CurTime(),
            }

            RunConsoleCommand("fnwvar_request_keyinfo", typeid)
        end

        dprint(string.format("fnwvar_update %s %s %s %i", Entity(entid), strkey, value, len))
    end
end)


net.Receive("fnwvar_remove", function(len)
    local entid   = net.ReadInt(EntIDBits)
    local strkey  = nil

    if (len == EntIDBits) then -- Remove all keys on this entity
        _FNWVars.Entities[entid] = nil
    else
        strkey = net.ReadString()
        local entkeys = _FNWVars.Entities[entid]
        if (entkeys) then
            entkeys[strkey] = nil
        end
    end

    dprint(string.format("fnwvar_remove %s %s %i", Entity(entid), strkey, len))
end)


net.Receive("fnwvar_keymap", function(len)
    local intkey  = net.ReadUInt(UIntKeyBits)
    local strkey  = net.ReadString()

    _FNWVars.KeyLookup[strkey] = intkey
    _FNWVars.KeyLookup[intkey] = strkey

    for entid, pendingkeys in pairs(_FNWVars.PendingFNWVars) do
        for index, key in pairs(pendingkeys) do
            if ((key.strkey == strkey) or (key.intkey == intkey)) then
                -- This key has a match now
                ReceivedFNWVarData(entid, strkey or key.strkey, key.value)
                _FNWVars.PendingFNWVars[entid][index] = nil
            end
        end
    end

    dprint(string.format("fnwvar_keymap %i %s %i", intkey, strkey, len))
end)
