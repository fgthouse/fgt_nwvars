--- Initial Shared Code for FNWVars.
-- @module FNWVars.Shared

FNWVars  = {}             -- Global FNWVars library table
_FNWVars = _FNWVars or {} -- Internal data table to be used for FNWVars

_FNWVars.TypeIDBits  = 4  -- Number of bits the uint type ids will be
_FNWVars.UIntKeyBits = 12 -- Number of bits the uint unique ids will be (4096 possible keys)
_FNWVars.EntIDBits   = 14 -- Number of bits the entindex will be (entity limit is 8k serverside and 8k networked)

_FNWVars.KeyLookup = _FNWVars.KeyLookup or {} -- Lookup table for matching string or integer key identifiers (Key can be String or Integer)

_FNWVars.TypeLookup = { -- Lookup table for matching string or integer type identifiers (Key can be String or Integer)
    ["Angle"]   = 1,
    ["Entity"]  = 2,
    ["VMatrix"] = 3,
    ["Vector"]  = 4,
    ["table"]   = 5, -- Color is a table
    ["boolean"] = 6,
    ["number"]  = 7,
    ["string"]  = 8,
    ["nil"]     = 9
}

-- Add the integer key pairs for doing int->str lookups
for typestr, typeid in pairs(_FNWVars.TypeLookup) do
    if (type(typestr) == "string") then
        _FNWVars.TypeLookup[typeid] = typestr
    end
end

_FNWVars.NetIO = { -- Table of type strings paired with a table for read/writing with the net library
    ["Angle"] = {
        read  = net.ReadAngle,
        write = net.WriteAngle
    },
    ["Entity"] = {
        read  = net.ReadEntity,
        write = net.WriteEntity
    },
    ["VMatrix"] = {
        read  = net.ReadMatrix,
        write = net.WriteMatrix
    },
    ["Vector"] = {
        read  = net.ReadVector,
        write = net.WriteVector
    },
    ["table"] = { -- Color is a table
        read  = net.ReadTable,
        write = net.WriteTable
    },
    ["boolean"] = {
        read  = net.ReadBool,
        write = net.WriteBool
    },
    ["number"] = {
        read  = net.ReadDouble,
        write = net.WriteDouble
    },
    ["string"] = {
        read  = net.ReadString,
        write = net.WriteString
    },
    ["nil"] = { -- Nil will be handled by just seeing this type across the network
        read = function()
        end,
        write = function(val)
        end
    }
}

-- Table of info for entities and their FNWVars
_FNWVars.Entities = _FNWVars.Entities or { --[[
    [entindex] = {
        ["fnwvar_key_string"] = {
            value             = value,                   -- Current value of this variable
            last_updated      = CurTime()                -- CurTime that we last received an update for this variable
            has_changes       = false                    -- True when this FNWVar has been changed (Serverside)
            recipients        = {player1, player2, ...}, -- Table of recipient players (Contains localplayer only on client)
            recipientfilter   = CRecipientFilter         -- Serverside only
        },
        ["money_printed"] = {
            ...
        }
    }
]]}

-- Table for storing callback functions
_FNWVars.Callbacks = _FNWVars.Callbacks or { --[[
    [string_key] = {
        [unique_callback_id] = [function],
        [unique_callback_id2] = [function2],
        ...
    },
    ...
]]}


-- Get the entity metatable for adding our metamethods
local fnwvar_debug_cvar = CreateConVar("fnwvars_debug", "0", FCVAR_ARCHIVE)
local EntityMetatable   = FindMetaTable("Entity")

function fnwvar_debug_print(...)
    if (fnwvar_debug_cvar:GetBool()) then
        print(...)
    end
end
local dprint = fnwvar_debug_print

--- Get a FNWVar from an entity.
-- @realm Shared
-- @param key A unique identifier string for the value we want to access
-- @param fallback A value of any type that will be returned if the given "key" isn't found
-- @return The value associated with "key", or fallback/nil if "key" doesn't exist
function EntityMetatable:GetFNWVar(key, fallback)
    assert(type(key) == "string", "ENTITY:GetFNWVar() called with an invalid key: " .. tostring(key))

    local index   = self:EntIndex()
    local fnwvars = _FNWVars.Entities[index]
    if (fnwvars) then
        local varinfo = fnwvars[key]
        if (varinfo and (varinfo.value ~= nil)) then
            return varinfo.value
        end
    end

    return fallback
end


--- Get the recipients for a FNWVar on an entity.
-- @realm Shared
-- @param key A unique identifier string for the value that we want to access's RecipientFilter
-- @return A table of players that will receive updates about this key
-- @return Serverside this will also return the RecipientFilter as a second return value
function EntityMetatable:GetFNWVarRecipients(key)
    assert(type(key) == "string", "ENTITY:GetFNWVarRecipients() called with an invalid key: " .. tostring(key))

    local index   = self:EntIndex()
    local fnwvars = _FNWVars.Entities[index]
    if (fnwvars) then
        local varinfo = fnwvars[key]
        if (varinfo and (varinfo.recipients ~= nil)) then
            return varinfo.recipients, varinfo.recipientfilter
        end
    end

    return {}, SERVER and RecipientFilter() or nil
end


--- Get a table of all the FNWVars associated with this entity.
-- @realm Shared
-- @return A table containing all FNWVar keys assigned to this entity, and its value/recipients.
function EntityMetatable:GetFNWVarTable()
    local index   = self:EntIndex()
    local fnwvars = _FNWVars.Entities[index]
    if (fnwvars) then
        return fnwvars
    end

    return {}
end


--- Checks if a given value is of a supported type for FNWVars
-- @realm Shared
-- @param value The value to have its type checked
-- @return True if the given value is of a supported type, false otherwise
function FNWVars.IsSupportedType(value)
    if (_FNWVars.TypeLookup[type(value)] ~= nil) then
        return true
    end

    return false
end


--- Add a callback function to call when a FNWVar changes.
-- @realm Shared
-- @param key The FNWVar key that we want to watch for changes
    -- <br/> If the key is "*" this callback will be executed for all changes
-- @param unique_id A unique string identifier for this callback
-- @tparam callback callback_function(entity, key, old_value, new_value)
    -- <br/>A function that will be called when a FNWVar of "key" changes
    -- <br/>Function is called with these parameters:
    --  <br/> * entity is the entity that this NWVar is associated with
    --  <br/> * key is the unique identifier for this FNWVar
    --  <br/> * old_value and new_value are the value before the update, and after the update
-- @return nil
function FNWVars.AddChangeCallback(key, unique_id, callback_function)
    assert(type(key)               == "string",   "FNWVars.AddChangeCallback() called with an invalid key: " .. tostring(key))
    assert(type(unique_id)         == "string",   "FNWVars.AddChangeCallback() called with an invalid unique_id: " .. tostring(unique_id))
    assert(type(callback_function) == "function", "FNWVars.AddChangeCallback() called without a callback_function")

    if (not _FNWVars.Callbacks[key]) then
        _FNWVars.Callbacks[key] = {}
    end
    _FNWVars.Callbacks[key][unique_id] = callback_function
end


--- Remove a callback function from a specific key
-- @realm Shared
-- @param key The unique identifier string for the callback we would like to remove
-- @param unique_id A unique string identifier for this callback
-- @return True on successful remove, false if callback doesn't exist
function FNWVars.RemoveChangeCallback(key, unique_id)
    assert(type(key)               == "string",   "FNWVars.RemoveChangeCallback() called with an invalid key: " .. tostring(key))
    assert(type(unique_id)         == "string",   "FNWVars.RemoveChangeCallback() called with an invalid unique_id: " .. tostring(unique_id))

    if (not _FNWVars.Callbacks[key]) then
        _FNWVars.Callbacks[key] = {}
    end
    if (_FNWVars.Callbacks[key][unique_id]) then
        _FNWVars.Callbacks[key][unique_id] = nil
        return true
    end
    return false
end


--- Returns a table of all callback functions for a key
-- @realm Shared
-- @param key The FNWVar key that we want to get the callbacks for
-- @return A table of all the callback functions for the given key
-- <br/> The returned table will have these members:
    -- <br/> * Key, the unique identifying string for this callback
    -- <br/> * Callback, the function being called when a FNWVar of "key" changes
function FNWVars.GetChangeCallbacks(key)
    assert(type(key) == "string", "FNWVars.GetChangeCallbacks() called with an invalid key: " .. tostring(key))

    if (not _FNWVars.Callbacks[key]) then
        _FNWVars.Callbacks[key] = {}
    end
    if (key == "*") then
        return _FNWVars.Callbacks
    end

    return _FNWVars.Callbacks[key]
end
