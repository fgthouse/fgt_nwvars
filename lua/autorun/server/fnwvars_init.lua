--- Initial Serverside Code for FNWVars.
-- @module FNWVars.Server

util.AddNetworkString("fnwvar_add")    -- Add sends the Entity, uinteger key, string key, type id, and a value of supported type
util.AddNetworkString("fnwvar_update") -- Update sends the Entity, uinteger key, type id, and a value of supported type
util.AddNetworkString("fnwvar_remove") -- Remove sends the Entity and the string key. String can be omitted to remove all keys for an entity
util.AddNetworkString("fnwvar_keymap") -- Keymap sends the uinteger key and its string key pair
local EntityMetatable = FindMetaTable("Entity") -- Get the entity metatable for adding our metamethods
local dprint          = fnwvar_debug_print
local UIntKeyBits     = _FNWVars.UIntKeyBits
local TypeIDBits      = _FNWVars.TypeIDBits
local EntIDBits       = _FNWVars.EntIDBits

_FNWVars.ClientState       = _FNWVars.ClientState or {}   -- This table will contain what the _FNWVars table should look like on every client
_FNWVars.HasChanges        = _FNWVars.HasChanges or false -- Set to true when something has changed
_FNWVars.ForceCheckSeconds = 5 -- Force check all entities for changes every X seconds


--- Creates a new integer key id for a given string key
-- @realm Server
-- @param strkey String Key to get/generate a integer key for
-- @return A integer key for the given key or the previous id if already exists. Nil otherwise.
local function GenerateKeyID(strkey)
    assert(type(strkey) == "string", "[FNWVars] GenerateKeyID called with a non-string key")
    assert(#strkey > 0,              "[FNWVars] GenerateKeyID called with an empty string key")

    local existingkey = _FNWVars.KeyLookup[strkey]
    if (existingkey) then -- String key already has an ID
        return existingkey
    end

    for k, v in pairs(_FNWVars.KeyLookup) do -- Check integer IDs as well
        if (strkey == v) then
            _FNWVars.KeyLookup[strkey] = k -- Add the corresponding string->integer lookup
            return k
        end
    end

    -- Key doesn't have a matching integer key yet. Create one.
    local newkey = #(_FNWVars.KeyLookup) + 1
    _FNWVars.KeyLookup[newkey] = strkey
    _FNWVars.KeyLookup[strkey] = newkey

    return newkey
end


--- Notifies a client that a entity's FNWVar/key will no longer be updated
-- @realm Server
-- @param ply The player to network the removal to
-- @param ent The entity that has a changed FNWVar
-- @param key The FNWVar string key
local function NotifyClientRemove(ply, ent, key)
    assert(IsValid(ply), "[FNWVars] NotifyClientRemove called with invalid player")
    assert(IsValid(ent), "[FNWVars] NotifyClientRemove called with invalid entity")
    assert(type(key) ==  "string", "[FNWVars] NotifyClientRemove called with invalid string")

    local entindex = ent:EntIndex()
    net.Start("fnwvar_remove")
        net.WriteInt(entindex, EntIDBits)
        net.WriteString(key)
    net.Send(ply)

    -- Update the client state to forget this FNWVar key
    local clientstate = _FNWVars.ClientState[ply:SteamID64()]
    if (clientstate) then
        local clkeys = clientstate.Entities[entindex]
        if (clkeys) then
            clkeys[key] = nil
        end
    else
        clientstate = {}
        clientstate.KeyLookup = {}
        clientstate.Entities  = {}
        clientstate.Player    = ply
        _FNWVars.ClientState[ply:SteamID64()] = clientstate
    end
end


--- Notifies a client of a new/updated FNWVar
-- @realm Server
-- @param ply The player to network the change to
-- @param ent The entity that has a changed FNWVar
-- @param key The FNWVar string key
-- @param value The FNWVar's new value
local function NotifyClient(ply, ent, key, value)
    assert(IsValid(ply), "[FNWVars] NotifyClient called with invalid player")
    assert(IsValid(ent), "[FNWVars] NotifyClient called with invalid entity")
    assert(#key > 0,     "[FNWVars] NotifyClient called with invalid/empty string")
    assert(FNWVars.IsSupportedType(value), "[FNWVars] NotifyClient called with an invalid type value: " .. type(value))

    local entindex    = ent:EntIndex()
    local playersid   = ply:SteamID64()
    local clientstate = _FNWVars.ClientState[playersid]
    if (not clientstate) then
        clientstate = {}
        clientstate.KeyLookup = {}
        clientstate.Entities  = {}
        clientstate.Player    = ply
        _FNWVars.ClientState[playersid] = clientstate
    end

    local keyid = GenerateKeyID(key) -- Integer key identifier for networking
    local clkeys = clientstate.Entities[entindex]
    if (not clkeys) then
        clkeys = {}
        clientstate.Entities[entindex] = clkeys
    end

    local clkeystate = clkeys[key]
    if (clkeystate) then
        if (clkeystate.recipientfilter) then -- Check if player is in the RecipientFilter, abort otherwise
            local inrecipient = false
            for _, rply in pairs(clkeystate.recipientfilter:GetPlayers()) do
                if (rply == ply) then
                    inrecipient = true
                end
            end
            if (not inrecipient) then
                dprint(string.format("[FNWVars] NotifyClient called with a player that isn't in the RecipientFilter: %s %s %s", ent, ply, key))
                return -- Abort
            end
        end

        -- Check if player knows value
        if (value == clkeystate.value) then
            dprint(string.format("[FNWVars] NotifyClient called with an unchanged value: %s %s %s %s", ent, ply, key, value))
            return
        end
    else
        clkeystate  = {}
        clkeys[key] = clkeystate
    end

    -- Network the change / new value to player
    local typestr = type(value)
    local typeid  = _FNWVars.TypeLookup[typestr]
    local typeio  = _FNWVars.NetIO[typestr]
    if (clientstate.KeyLookup[keyid]) then -- Player knows the integer key for networking
        net.Start("fnwvar_update")
            net.WriteInt(entindex, EntIDBits)
            net.WriteUInt(keyid, UIntKeyBits)
            net.WriteUInt(typeid, TypeIDBits)
            typeio.write(value)
        net.Send(ply)

    else -- Network this new FNWVar to the client
        net.Start("fnwvar_add")
            net.WriteInt(entindex, EntIDBits)
            net.WriteUInt(keyid, UIntKeyBits)
            net.WriteString(key)
            net.WriteUInt(typeid, TypeIDBits)
            typeio.write(value)
        net.Send(ply)

        -- Update client state
        clientstate.KeyLookup[keyid] = key
        clientstate.KeyLookup[key]   = keyid
    end

    -- Update/add client state key info
    clkeystate.value        = value
    clkeystate.last_updated = CurTime()
end


--- Iterates all pending changes and networks to clients that should know about the changes
-- @realm Server
-- @param force_all Forces all entities to be checked
local function NetworkChangesToClients(force_all)
    if (not force_all and not _FNWVars.HasChanges) then return end

    for entindex, keystable in pairs(_FNWVars.Entities) do -- Check all entities with FNWVars on them
        for keystring, keyinfo in pairs(keystable) do -- Check all FNWVar keys on this entity
            if (not force_all and not keyinfo.has_changes) then continue end

            local ent = Entity(entindex)
            if (IsValid(ent)) then
                if (not keyinfo.recipientfilter) then
                    keyinfo.recipientfilter = RecipientFilter()
                end

                local players = keyinfo.recipientfilter:GetPlayers()
                if (players) then
                    local recipientslookup = {}
                    for _, recipientply in pairs(players) do -- Generate a lookup table for faster checks
                        if (IsValid(recipientply)) then
                            recipientslookup[recipientply] = recipientply
                        end
                    end

                    for plysid, plystate in pairs(_FNWVars.ClientState) do -- Check all clients to see what they need to know about
                        local ply = plystate.Player
                        if (IsValid(ply)) then
                            local clkeys = plystate.Entities[entindex] -- Client knowledge of this entity's key/value
                            if (not clkeys) then -- Create empty table for this entity's clientstate keys if it doesn't exist
                                clkeys = {}
                                plystate.Entities[entindex] = clkeys
                            end

                            local clkeystate = clkeys[keystring] -- The state of this FNWVar key on the client
                            if (recipientslookup[ply]) then
                                -- This player should know about this entity's FNWvar key
                                if (not clkeystate) then -- This player doesn't know anything about this fnwvar key, but they should
                                    NotifyClient(ply, ent, keystring, keyinfo.value) -- Tell the client about this new FNWVar
                                    continue
                                elseif (clkeystate.value ~= keyinfo.value) then -- This player needs the updated value
                                    NotifyClient(ply, ent, keystring, keyinfo.value)
                                    continue
                                end
                            else
                                -- This player shouldn't know anything about this FNWVar/key
                                if (clkeystate) then -- This player knows something it shouldn't
                                    NotifyClientRemove(ply, ent, keystring)
                                    continue
                                end
                            end
                        else
                            dprint(string.format("[FNWVars] NetworkChangesToClients called and invalid player found: %s", plystate.Player))
                            _FNWVars.ClientState[plysid] = nil
                        end
                    end

                    keyinfo.recipients = players -- Update recipients table
                else
                    dprint(string.format("[FNWVars] NetworkChangesToClients called but entity %s has no recipientfilter", ent))
                end
            else
                dprint(string.format("[FNWVars] NetworkChangesToClients called and invalid entity found: %s [%i]", ent, entindex))
                net.Start("fnwvar_remove") -- Entity is invalid, tell all players its gone
                    net.WriteInt(entindex, EntIDBits)
                net.Broadcast()
                _FNWVars.Entities[entindex] = nil

                -- Update client states as well
                for plysid, plystate in pairs(_FNWVars.ClientState) do
                    if (plystate.Entities and plystate.Entities[entindex]) then
                        plystate.Entities[entindex] = nil
                    end
                end
            end

            keyinfo.has_changes = false
        end
    end

    _FNWVars.HasChanges = false
end


--- Set a FNWVar on an entity.
-- @realm Server
-- @param key A unique identifier string for the given value
-- @param value A value of any supported type
-- @return The previous value of "key" (if applicable) on success, nil otherwise
function EntityMetatable:SetFNWVar(key, value)
    assert(type(key) == "string",          "ENTITY:SetFNWVar() called with an invalid key: " .. tostring(key))
    assert(#key > 0,                       "ENTITY:SetFNWVar() called with an empty string key")
    assert(FNWVars.IsSupportedType(value), "ENTITY:SetFNWVar() called with an invalid type value: " .. type(value))

    if (key == "*") then return nil end -- Disallow wildcard keys, used in callbacks

    if (not FNWVars.IsSupportedType(value)) then
        return
    end

    local index   = self:EntIndex()
    local fnwvars = _FNWVars.Entities[index]
    if (not fnwvars) then -- Create table for this entity
        fnwvars = {}
        _FNWVars.Entities[index] = fnwvars
        _FNWVars.HasChanges = true
    end

    local varinfo = fnwvars[key] -- Create key table for this entity
    if (not varinfo) then
        varinfo = { -- Populate default values for this key
            value           = value,            -- Current value of this variable
            last_updated    = CurTime(),        -- CurTime that we last received an update for this variable
            has_changes     = true,             -- FNWVar has been changed (created)
            recipients      = {},               -- Table of recipient players
            recipientfilter = RecipientFilter() -- RecipientFilter of players
        }
        fnwvars[key] = varinfo

        _FNWVars.HasChanges = true
        dprint(string.format("ENT:SetFNWVar is creating a new key %s with value %s on entity %s", key, value, self))
    end

    local oldvalue = nil
    if (varinfo.value ~= value) then
        oldvalue             = varinfo.value
        varinfo.value        = value
        varinfo.last_updated = CurTime()
        varinfo.has_changes  = true

        _FNWVars.HasChanges = true
        dprint(string.format("ENT:SetFNWVar is updating key %s with value %s on %s at %f", key, value, self, CurTime()))

        -- Execute callbacks
        for unique_id, callfunc in pairs(_FNWVars.Callbacks["*"] or {}) do -- Wildcard callbacks
            callfunc(self, key, oldvalue, value)
        end
        local callbacks = _FNWVars.Callbacks[key]
        if (callbacks) then
            for unique_id, callfunc in pairs(callbacks) do
                callfunc(self, key, oldvalue, value)
            end
        end
    end

    return oldvalue
end


--- Set the recipients for a FNWVar on an entity.
-- @realm Server
-- @param key A unique identifier string for the value that will have its RecipientFilter changed
-- @param filter A CRecipientFilter of the players that should receive updates on this key/value
-- @return A table of players that will receive updates about this key
function EntityMetatable:SetFNWVarRecipients(key, filter)
    assert(type(key) == "string",              "ENTITY:SetFNWVarRecipients() called with an invalid key: " .. tostring(key))
    assert(#key > 0,                           "ENTITY:SetFNWVarRecipients() called with an empty string key")
    assert(type(filter) == "CRecipientFilter", "ENTITY:SetFNWVarRecipients() called with an invalid filter: " .. tostring(filter))

    local index   = self:EntIndex()
    local fnwvars = _FNWVars.Entities[index]
    if (not fnwvars) then -- Create table for this entity
        fnwvars = {}
        _FNWVars.Entities[index] = fnwvars
        _FNWVars.HasChanges = true
    end

    local varinfo = fnwvars[key] -- Create key table for this entity
    if (not varinfo) then
        varinfo = { -- Populate default values for this key
            value        = nil,       -- Current value of this variable
            last_updated = CurTime(), -- CurTime that we last received an update for this variable
            has_changes  = true,
        }
        fnwvars[key] = varinfo

        _FNWVars.HasChanges = true
        dprint(string.format("ENT:SetFNWVarRecipients is creating a new key %s on entity %s", key, self))
    end

    local newrecipients = filter:GetPlayers()
    if (varinfo.recipients and (#varinfo.recipients ~= #newrecipients)) then
        _FNWVars.HasChanges = true
    else
        local recipientlookup = {} -- Create a lookup table for the recipients
        for _, rply in pairs(newrecipients) do
            recipientlookup[rply] = rply
        end

        -- Check if any new players are in the recipients filter
        for _, rply in pairs(varinfo.recipients or {}) do
            if (not recipientlookup[rply]) then
                _FNWVars.HasChanges = true
                break
            end
        end
    end

    if (_FNWVars.HasChanges == true) then
        varinfo.has_changes     = true
        varinfo.recipients      = newrecipients -- Table of recipient players
        varinfo.recipientfilter = filter        -- RecipientFilter of players
    end

    return varinfo.recipients
end


-- Force check all entities for changes on an interval
timer.Create("FNWVar_Timer_CheckAllEntities", _FNWVars.ForceCheckSeconds, 0, function()
    NetworkChangesToClients(true)
end)


-- Network any FNWVar changes once every tick
hook.Add("Tick", "FNWVars_Tick_NetworkPendingChanges", function()
    NetworkChangesToClients()
end)


-- Tell all clients to forget about an entity if it is removed and had FNWVars on it
hook.Add("EntityRemoved", "FNWVars_EntRemoved_NetworkEntityDeath", function(ent)
    local entindex = ent:EntIndex()
    if (entindex and _FNWVars.Entities[entindex]) then
        net.Start("fnwvar_remove") -- Entity is invalid, tell all players its gone
            net.WriteInt(entindex, EntIDBits)
        net.Broadcast()
        _FNWVars.Entities[entindex] = nil

        -- Update client states as well
        for plysid, plystate in pairs(_FNWVars.ClientState) do
            if (plystate.Entities and plystate.Entities[entindex]) then
                plystate.Entities[entindex] = nil
            end
        end
    end
end)


-- Create a ClientState table for this new player
hook.Add("PlayerInitialSpawn", "FNWVars_PlyInitSpawn_CreateClientTable", function(ply)
    local sid = ply:SteamID64()

    if (not _FNWVars.ClientState[sid]) then
        local state = {}
        state.KeyLookup = {}
        state.Entities  = {}
        state.Player    = ply

        _FNWVars.ClientState[sid] = state
    end
end)


-- Clear a ClientState table for this player
hook.Add("PlayerDisconnected", "FNWVars_PlyDisconnected_ClearClientTable", function(ply)
    local sid = ply:SteamID64()

    _FNWVars.ClientState[sid] = nil
end)


-- Console command that will be called if a client doesn't know what strkey goes with an intkey or vice-versa.
local request_delay   = 10 -- Number of seconds a player must wait before
local requests_lookup = {} -- Table for storing the next time a player can make a request for a keyid/keystring
concommand.Add("fnwvar_request_keyinfo", function(ply, cmd, args)
    local lookup_arg = args[1]
    if (not lookup_arg) then return end
    if (not IsValid(ply)) then return end

    -- Rate limiting
    local plysid       = ply:SteamID64()
    local ply_requests = requests_lookup[plysid]
    if (not ply_requests) then
        ply_requests = {}
        requests_lookup[plysid] = ply_requests
    end

    local next_request = ply_requests[lookup_arg]
    local curtime      = CurTime()
    if (next_request and (next_request > curtime)) then
        ply_requests[lookup_arg] = curtime + request_delay
        dprint(string.format("[FNWVars] fnwvar_request_keyinfo received from %s for %s too quickly, ignoring", ply, lookup_arg))
        return
    end

    -- Lookup and return keystring and keyid if found
    local keyid  = tonumber(lookup_arg)
    local keystr = nil
    if (not keyid) then
        keystr = lookup_arg
        keyid = _FNWVars.KeyLookup[keystr]
    else
        keystr = _FNWVars.KeyLookup[keyid]
    end

    if (_FNWVars.KeyLookup[keyid] and _FNWVars.KeyLookup[keystr]) then
        net.Start("fnwvar_keymap")
            net.WriteUInt(keyid, UIntKeyBits)
            net.WriteString(keystr)
        net.Send(ply)
    end

    ply_requests[lookup_arg] = curtime + request_delay
end)
